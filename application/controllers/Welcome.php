<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->library('javascript');
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->view('index');

	}
	function login()
	{
		$this->load->view('login');
	}
	function signup()
	{
		$this->load->library('session');
		$this->load->view('signup');
	}
	function home()
	{
	  $this->load->model('user_model');
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->database();
		$this->load->library('form_validation');
		$user_id= $this->user_model->get_userID($this->input->post('nama'));
		$sessiondata = array(
				 'loginuser' => TRUE,
				 'id'=>$user_id
		);
		$this->session->set_userdata($sessiondata);
		$this->load->view('home');
	}
}

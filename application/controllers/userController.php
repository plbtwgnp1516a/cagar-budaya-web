                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  <?php if(!defined('BASEPATH')) exit('No direct script allowed');

class UserController extends CI_Controller{

  public function __construct()
  {
       parent::__construct();
       $this->load->library('session');
       $this->load->helper('form');
       $this->load->helper('url');
       $this->load->helper('html');
       $this->load->database();
       $this->load->library('form_validation');
       //load the login model
       $this->load->model('user_model');
  }
  public function index()
  {

    $username = $this->input->post("nama");
    $password = $this->input->post("password");

    $this->form_validation->set_rules("nama", "Username", "trim|required");
    $this->form_validation->set_rules("password", "Password", "trim|required");

    if ($this->form_validation->run() == FALSE)
    {
         //validation fails
         $this->load->view('login');
    }
    else
    {
         //validation succeeds
         if ($this->input->post('btn_login') == "Login")
         {
              //check if username and password is correct
              $usr_result = $this->user_model->get_user($username, $password);
              if ($usr_result > 0) //active user record is present
              {
                 $user_id= $this->user_model->get_userID($this->input->post('nama'));
                   //set the session variables
                   $sessiondata = array(
                        'username' => $username,
                        'loginuser' => TRUE,
                        'id'=>$user_id
                   );
                   $this->session->set_userdata($sessiondata);



                   $this->session->keep_flashdata('id', $user_id);
                   redirect("Welcome/home");
              }
              else
              {
                   $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid email and password!</div>');
                   redirect('UserController/index');
              }
         }
         else
         {
              redirect('UserController/index');
         }
    }
  }

  public function signup()
  {
    $this->load->library('form_validation');
    $this->load->model('User_model');

      $data=array(
        'nama'=>$this->input->post('username'),
        'email'=>$this->input->post('email'),
        'password'=>$this->input->post('password'),
        'telepon'=>$this->input->post('telpon')
      );


      if(($this->input->post('password'))==($this->input->post('password_confirmation')))
      {
          $result=$this->User_model->addUser($data);
          if($result==TRUE)
          {
              $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Registrasi Berhasil ! Silahkan login untuk melanjutkan</div>');
              redirect("UserController/index");
          }
          else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Email sudah ada</div>');
            redirect("Welcome/signup");
          }
      }
      else {
          $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">password dan kombinasi password tidak cocok !</div>');
          redirect("Welcome/signup");
      }
  }

  public function addapp()
  {
      $this->load->model('User_model');
      $this->load->helper('string');
    	$user_id= $this->user_model->get_userID($this->input->post('nama'));
      $data=array(
        'nama_aplikasi'=>$this->input->post('nama_aplikasi'),
        'deskripsi'=>$this->input->post('deskripsi'),
        'api_key'=>random_string('alnum',20),
        'id_user'=>$user_id);
        $appi=$data['api_key'];
        $this->User_model->addApp($data);
        $this->session->set_flashdata('msg',$appi);
        $this->session->set_flashdata('alamat','http://toniindrawan.esy.es/plbtw_uas_a/public/api/cagarbudaya/1/1?api_key=');
        $this->session->set_flashdata('struktur', '
        <div class="row">
        <div class="col-md-12">
        {<br>
          <div class="a">
          "content": [<br>
          </div>
            <div class="b">
            {<br>
            </div>
                <div class="c">
                  "id_cagar": "1",<br>
                  "nama_cagar": "Candi Prambanan",<br>
                  "deskripsi": "Ini candi prambanan. Suatu candi peninggalan hindu. Candi ini terletak di kota Yogyakarta.
                  Banyak wisatawan yang berkunjung kesini. Akan terapi, terdapat mitos bahwa pasangan yang datang ke candi prambanan akan putus hubungan.",<br>
                  "tahun_peninggalan": "1999",<br>
                  "nama_jenis": "Candi",<br>
                  "longitude": "456",<br>
                  "latitude": "123",<br>
                  "alamat": "Yogyakarta",<br>
                  "status": "",<br>
                  "id_user": "1",<br>
                  "nama": "deni",<br>
                  "email": "deni@gmail.com",<br>
                  "telepon": "082373919466",<br>
                  "poin": "10",<br>
                  "multimedia": [<br>
                  </div>
                      <div class="d">
                      {<br>
                      </div>
                          <div class="e">
                          "id_multimedia": "1",<br>
                          "nama_multimedia": "google",<br>
                          "url": "http:\/\/google.com",<br>
                          "tipe": "123",<br>
                          "nama": "deni",<br>
                          "email": "deni@gmail.com",<br>
                          "telepon": "082373919466",<br>
                          "poin": "10"<br>
                          </div>
                      },<br>
                      <div class="d">
                      {<br>
                      </div>
                          <div class="e">
                          "id_multimedia": "2",<br>
                          "nama_multimedia": "Dege",<br>
                          "url": "http:\/\/dewadg.net",<br>
                          "tipe": "123",<br>
                          "nama": "deni",<br>
                          "email": "deni@gmail.com",<br>
                          "telepon": "082373919466",<br>
                          "poin": "10"<br>
                          </div>
                      <div class="d">
                      }<br>
                      </div>
                  <div class="c">
                  ]<br>
                  </div>
              <div class="b">
              }<br>
              </div>
          <div class="a">
          ],<br>
          "totalPages": 9<br>
          </div>
      }
      <div>
      </div><br><hr>');
        redirect("Welcome/home");
  }
  public function logout()
  {
    $this->session->sess_destroy();
    redirect('Welcome');
  }
}

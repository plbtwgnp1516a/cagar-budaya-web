<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

  Class User_model extends CI_Model{
    function __construct()
    {
      parent::__construct();
    	$this->load->database();
    }
    function get_userID($email)
    {
        $this->db->where('email',$email);
        $query = $this->db->get('user_public');
        $user_id=0;
        foreach ($query->result() as $row)
        {
            $user_id = $row->id;
        }
        return $user_id;
    }
    function get_user($usr, $pwd)
    {
         $sql = "select * from user_public where email = '" . $usr . "' and password = '" . $pwd . "'";
         $query = $this->db->query($sql);
         return $query->num_rows();
    }
    function addUser($data)
    {
        $condition="email=" . "'" . $data['email'] . "'";
        $this->db->select('*');
        $this->db->from('user_public');
        $this->db->where($condition);
        $this->db->limit(1);
        $query=$this->db->get();
        if($query->num_rows()==0)
        {
            $this->db->insert('user_public',$data);
            if ($this->db->affected_rows() > 0) {
              return true;
            }
        }
        else{
          return false;
        }
    }
    function addApp($data){
        $this->db->insert('aplikasi',$data);
    }
  }

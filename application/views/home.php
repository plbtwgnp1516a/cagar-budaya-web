<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo link_tag('css/form-elements.css')?>
    <?php echo link_tag('css/bootstrap.min.css')?>



    <body>

 <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
         <div class="row">
			    <div class="col-sm-4 col-sm-offset-8">
       		  <ul class="nav navbar-nav navbar-right">
                 <li><a href="<?php echo site_url('UserController/logout');?>">Log Out</a></li>
        	  </ul>
          </div>
         </div>
    </div><!-- /.navbar-collapse -->
  </div>
<!-- /.container-fluid -->
</nav>
<section class="container content-section">
    <div class="row">
      <div class="col-md-6 form-box">
          <div class="konten">
            <center><div><h1>Cagar Budaya API<h1></div></center>
            <hr>
            <br><br><br>
            <div class="form-bottom">
              <form method="post" action="<?=base_url()?>index.php/userController/addapp">
                <input type="hidden" id="nama" name="nama" placeholder="Deskripsi Aplikasi" required class="form-username form-control" value="<?php echo $this->session->userdata('username'); ?>">
                </br>
                <input type="text" id="nama_aplikasi" name="nama_aplikasi" placeholder="Nama Aplikasi" required class="form-username form-control">
                </br>
                <input type="text" id="deskripsi" name="deskripsi" placeholder="Deskripsi Aplikasi" required class="form-username form-control">



                </br>
                </br>
                <center><button class="btn btn-default" type="submit" value="signup">Dapatkan Api Key</button></center>
              </form>
              <br><br><br>
              <hr>
            </div>

          </div>
          <script src="/js/bootstrap.min.js"></script>
            <!-- Javasript Files required for page-->
    </div>
    <div class="well col-md-6">
      <div class="konten">
        <div>Api Key : <?php echo $this->session->flashdata('msg'); ?></div>
        <hr>
        <div>Alamat API : <?php echo $this->session->flashdata('alamat'); ?><?php echo $this->session->flashdata('msg'); ?></div>
        <hr>
        <div>
        Struktur : <br><br>
        <h20><?php echo $this->session->flashdata('struktur'); ?><h20>
        </div>
      </div>
    </div>
</div>
</section>
<div class="row">
  <div class="col-lg-12">
    <div class="z">
      <div class="well col-md-12">
      </br>

      <div><h11>Langkah - langkah :</h11></div>
      <li>Daftarkan aplikasi terlebih dahulu melalui form di atas.</li>
      <br>
      <li>Kemudian akan tampil API Key dari aplikasi yang telah anda daftarkan.</li>
      <br>
      <li>Untuk mengakses API dapat dilakukan melalui alamat yang akan tampil pada halaman disamping.</li>
      <br>
      <div class="well col-md-12">
        <div>Untuk alamatnya : </div>
        <hr>
        <div>/cagarbudaya/<h100>page</h100>/<h101>itemperpage</h101>?api_key=</div>
      </div>
      <br>
      <li>Silahkan masukkan API Key yang didapatkan pada alamat yang tersedia.</li>
    </div>
    </div>
  </div>
</div>
<section class="container footer-section text-center">
	<h2>	&copy; PLBTW Kelompok A</h2>
</section>

   		    <div id="myModal" class="modal">

 			 <!-- Modal content -->
              <div class="modal-content">
                <span class="close">×</span>
                <p>Your API Key : </p>
              </div>

            </div>
            <script>
// Get the modal
			var modal = document.getElementById('myModal');

			// Get the button that opens the modal
			var btn = document.getElementById("myBtn");

			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("close")[0];

			// When the user clicks the button, open the modal
			btn.onclick = function() {
				modal.style.display = "block";
			}

			// When the user clicks on <span> (x), close the modal
			span.onclick = function() {
				modal.style.display = "none";
			}

			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
				if (event.target == modal) {
					modal.style.display = "none";
				}
			}
			</script>
    <!--/#footer-->

</body>
</html>

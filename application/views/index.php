<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo link_tag('css/bootstrap.min.css')?>
    <?php echo link_tag('css/animate.css')?>
    <?php echo link_tag('css/modal.css')?>
    <?php echo link_tag('css/form-elements.css')?>
    <?php echo link_tag('css/modern-business.css')?>

    <body>
 <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
         <div class="row">
			<div class="col-sm-4 col-sm-offset-8">
       		  <ul class="nav navbar-nav navbar-right">
			     <li><a href="<?php echo site_url('UserController/index');?>">Login</a></li>
           <li><a href="<?php echo site_url('Welcome/signup');?>">Sign Up</a></li>
        	  </ul>
          </div>
        </div>
      </div>
    </div><!-- /.navbar-collapse -->
<!-- /.container-fluid -->
</nav>
<header id="myCarousel" class="carousel slide">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <div class="fill" style="background-image:url('images/1.jpg');"></div>
        <div class="carousel-caption">
        </div>
    </div>
    <div class="item">
      <div class="fill" style="background-image:url('images/2.jpg');"></div>
        <div class="carousel-caption">

        </div>
    </div>
    <div class="item">
      <div class="fill" style="background-image:url('images/3.jpg');"></div>
        <div class="carousel-caption">

        </div>
    </div>
  </div>
  <!-- Controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="icon-prev"></span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="icon-next"></span>
  </a>
</header>
<div class="container">

        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Selamat Datang di Cagar Budaya Web
                </h1>
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-check"></i>Apa itu Cagar Budaya Web ?</h4>
                    </div>
                    <div class="panel-body">
                        <p>
                          Cagar budaya web merupakan website yang digunakan oleh anda para developer untuk mendapatkan API Key yang akan digunakan untuk
                          membuat aplikasi berdasarkan web service yang kami miliki.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-gift"></i>Mendapatkan API Key?</h4>
                    </div>
                    <div class="panel-body">
                        <p>Silahkan signup dan login untuk mendapatkan API Key</p>

                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- Portfolio Section -->

        <!-- /.row -->

        <!-- Features Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Yang tersedia di Cagar Budaya API</h2>
            </div>
            <div class="col-md-6">
                <ul>

                    <li>jQuery v1.11.0</li>
                    <li>Font Awesome v4.1.0</li>
                    <li>Working PHP contact form with validation</li>
                    <li>Unstyled page elements for easy customization</li>
                    <li>17 HTML pages</li>
                </ul>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.</p>
            </div>
            <div class="col-md-6">
                <img class="img-responsive" src="images/4.jpg" alt="">
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <!-- Call to Action Section -->


        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; PLBTW Kelompok A</p>
                </div>
            </div>
        </footer>

    </div>



    <script src="js/jquery.js"></script>
    <?php echo link_tag('js/jquery.js')?>
  <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <?php echo link_tag('js/bootstrap.min.js')?>
    <script>
      $('.carousel').carousel({
          interval: 5000 //changes the speed
      })
    </script>


    <!--/#footer-->

</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo link_tag('css/form-elements.css')?>
    <?php echo link_tag('css/bootstrap.min.css')?>
    <?php echo link_tag('css/animate.css')?>
    <?php echo link_tag('css/modal.css')?>



    <body>
 <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
         <div class="row">
			<div class="col-sm-4 col-sm-offset-8">
       		  <ul class="nav navbar-nav navbar-right">
                 <li><a href="<?php echo site_url('/');?>">Home</a></li>
                 <li><a href="<?php echo site_url('Welcome/signup');?>">Sign Up</a></li>
        	  </ul>
          </div>
        </div>
      </div>
    </div><!-- /.navbar-collapse -->
<!-- /.container-fluid -->
</nav>
<section class="container content-section text-center">
<div class="row">
			<div class="col-md-6 form-box">
                        <div class="verticalLine">
                    	<div class="konten">
                      <br><br><br>
           				<div><h1>LOGIN<h1></div>

                    	<div class="form-bottom">
                          <?php echo $this->session->flashdata('msg'); ?>
                          <form method="post" action="<?=base_url()?>index.php/userController/index">

                            <input type="text" id="nama" name="nama" placeholder="Email" class="form-control">
                            </br>

                            <input type="password" id="password" name="password" placeholder="Password" class="form-control">
                            </br>

                            <center><input id="btn_login" name="btn_login" type="submit" class="btn" value="Login" /></center>

                            <div>
                            <!--Display the signup Link-->

                    		</form>

						</div>
                    </div>
      	<br><br><br><br><br><br>
		<!-- Javasript Files required for page-->
		<script src="/js/bootstrap.min.js"></script>
			</div>
            </div>
			</div>
		</div>
        <br><br><br><br><br><br><br><br>
</section>
<section class="container footer-section text-center">
	<h2>	&copy; PLBTW Kelompok A</h2>
</section>

    <!--/#footer-->

</body>
</html>
